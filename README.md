# IDU ODU 300 - monitor
Programik łączy się z IDU lub ODU za pomocą telnetu. Wykorzystując zestaw komend ustawia najwyższy stopień logowania i zapisuje ów logi do rotujących plików tekstowych.

Ma to pomóc debugować problemy z działaniem / stabilnością urządzenia.

``config.ini`` - plik konfiguracyjny, w którym można zdefiniować konkretne komendy konfigurujące stan ODU i IDU

## Instalator dla Windowsa
Dla Windowsa jest możliwość wygenerowania instalatora z plikiem _*.exe_, który embedduje pythona i potrzebne paczki:
```bash
python cx.py bdist_msi
```