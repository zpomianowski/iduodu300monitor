#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from cx_Freeze import setup, Executable

VERSION = '0.0.2'

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "excludes": ["tkinter"],
    "optimize": 2,
    "append_script_to_exe": True,
    "include_in_shared_zip": True}

shortcut_table = [
    ("DesktopShortcut",                    # Shortcut
     "DesktopFolder",                      # Directory_
     "iduodu300monitor",                   # Name
     "TARGETDIR",                          # Component_
     "[TARGETDIR]\iduodu300monitor.exe",   # Target
     None,                                 # Arguments
     None,                                 # Description
     None,                                 # Hotkey
     None,                                 # Icon
     None,                                 # IconIndex
     None,                                 # ShowCmd
     "TARGETDIR"                           # WkDir
     )]

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
options = {
    "build_exe": build_exe_options,
    }

if sys.platform == "win32":
    options = {
        "build_exe": build_exe_options,
        "bdist_msi": {
            "initial_target_dir": "c:\iduodu300monitor",
            "data": {"Shortcut": shortcut_table}
            }
        }

setup(name="iduodu300monitor",
      version=VERSION,
      description="IDU ODU 300 MONITOR",
      options=options,
      executables=[Executable("main.py", targetName="iduodu300monitor.exe")])
