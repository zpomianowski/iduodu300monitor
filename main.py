#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import signal
import time
import socket
import argparse
import urllib2
import logging
from logging.handlers import RotatingFileHandler
from Queue import Queue, Empty
from threading import Thread
from telnetlib import Telnet
import ConfigParser


CFG = ConfigParser.ConfigParser()
CFG.readfp(open('config.ini', 'r'))


if sys.platform == "win32":
    import win_inet_pton


def get_logger(name='idu'):
    logfmt = "%(asctime)s [%(levelname)-5.5s]  %(message)s"
    logging.basicConfig(level=10, format=logfmt)
    logger = logging.getLogger()
    logfilepath = os.path.join(os.getcwd(), '%s.log' % name)
    rothanlder = RotatingFileHandler(
        logfilepath, maxBytes=1024*1024*200, backupCount=99)
    rothanlder.setFormatter(logging.Formatter(logfmt))
    logger.addHandler(rothanlder)
    return logger


def signal_handler(signal, frame):
    print '\nOjoj! Ctrl+C pressed and bum! Logging stopped!!!'
    sys.exit(0)


def wait_for_odu(ip):
    while True:
        try:
            urllib2.urlopen('http://%s' % ip, timeout=10)
            return
        except urllib2.URLError:
            pass


def worker():
    while True:
        try:
            item = q.get_nowait()
            l.debug('CMD: %s' % item)
            tn.write(item + '\n')
            q.task_done()
        except Empty:
            pass
        data = tn.read_until("\n", timeout=5)
        if data:
            l.info(data.strip())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=(
            'Launches logging for IDU300 or ODU300'),
        prog='iduodu300ltemonitor')
    parser.add_argument('ip', nargs='?',
                        default='192.168.0.1',
                        help='default: idu -> 192.168.0.1, odu -> 192.168.100.1')
    parser.add_argument('--odu', dest='odu_mode', action='store_true',
                        help='default logging for IDU - '
                             'change to ODU')
    parser.add_argument('-u', '--user', dest='user',
                        default='admin',
                        help='default: idu -> admin, odu -> root')
    parser.add_argument('-p', '--password', dest='passwd', metavar='PASSWORD',
                        default='admin',
                        help='default: idu -> admin, odu -> gct')

    args = parser.parse_args()
    l = get_logger('odu' if args.odu_mode else 'idu')
    if args.odu_mode and args.user == 'admin' and args.passwd == 'admin':
        args.user = 'root'
        args.passwd = 'gct'
    if args.odu_mode and args.ip == '192.168.0.1':
        args.ip = '192.168.100.1'
    try:
        socket.inet_pton(socket.AF_INET, args.ip)
    except socket.error as err:
        l.error('Nieprawny adres IP: ' + str(err))

    tn = Telnet()
    l.debug('Connection attempt %s ...' % args.ip)
    tn.open(args.ip, timeout=10, port=23)
    tn.read_until('login: ')
    tn.write(args.user + '\n')
    tn.read_until('word: ')
    tn.write(args.passwd + '\n')

    tag = 'odu' if args.odu_mode else 'idu'
    pre_cmds = []
    cmds = [c.strip() for c in filter(None, CFG.get(tag, 'cmds').splitlines())]
    callback_cmds = [c.strip() for c in
                     filter(None, CFG.get(tag, 'callback_cmds').splitlines())]

    try:
        idx = cmds.index('reboot')
        pre_cmds = cmds[:idx+1]
        cmds = cmds[idx+1:]
        print pre_cmds, cmds
    except ValueError:
        pass

    if pre_cmds:  
        for c in pre_cmds:
            l.debug('CMD: %s' % c)
            tn.write(c + '\n')
            time.sleep(1)

        wait_for_odu(args.ip)

        l.debug('Próba polączenia z %s ...' % args.ip)
        tn.open(args.ip, timeout=10, port=23)
        tn.read_until('login: ')
        tn.write(args.user + '\n')
        tn.read_until('word: ')
        tn.write(args.passwd + '\n')

    q = Queue()
    t = Thread(target=worker)
    t.daemon = True
    t.start()

    [q.put(c) for c in cmds]

    signal.signal(signal.SIGINT, signal_handler)
    print 'Press Ctrl+C to stop'
    while True:
        time.sleep(600)
        [q.put(c) for c in callback_cmds]